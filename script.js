var cart = new Array();

$(".shop__item_buy img").on("click", function(){
    var product_id = $(this).attr('data-buy');
    let newproduct = new Array(
        $(this).attr('data-buy'),
        $(this).attr('data-name'),
        $(this).attr('data-price'),
        $(this).attr('data-currency')
    );
    cart.push(newproduct);
    refresh_cart();
});

$("#clear_cart").on("click", function(){
    if(cart.length>0){
        cart.splice(0,cart.length);
    }
    refresh_cart();
});

function refresh_cart(){
    let drive_cart = "<div class='in_cart'>";
    for(i=0;i<cart.length;i++){
        drive_cart += "<div class='in_cart_item'><div class='span_10'><div class='in_cart_name'>"+cart[i][1]+"</div>"+"<div class='in_cart_price'>"+cart[i][2]+"<span class='in_cart_currency'> "+cart[i][3]+"</span></div>"+"</div><div class='span_2'><div class='in_cart_delete alignright' onclick='delete_product("+i+")'>Удалить</div></div></div>";
    }
    drive_cart += "<div class='in_cart_products_count mb_2'>Товаров в корзине: "+cart.length+"</div></div>";
    $('#in_cart').html(drive_cart);
}

function delete_product(product_id){
    cart.splice(product_id,1);
    refresh_cart();
}