<?php
//я тут создаю массив с данными (имитирую получение днаннных из БД в массив)
$_ITEMS = array(
    '1' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '2' =>  array(
        'name' =>  'Бокал',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '3' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '4' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '5' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '6' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '7' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '8' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '9' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
    '10' =>  array(
        'name' =>  'Декантер WINEWINGS 0,85 л Riedel (2007/02 S1)',
        'brand' =>  'Riedel | DECANTER',
        'price' =>  '12250',
        'currency' =>  'грн',
        'img' =>  './img/1.png',
        'img_alt' =>  'Riedel | DECANTER',
    ),
);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Корзина</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    </head>
    <body>
        <header class="container mb_2">
            <div class="row">
                <div class="span_10">
                    <h1>Корзина</h1>
                </div><div class="span_2 alignright">
                    <span id="clear_cart">Очистить корзину</span>
                </div>
                <div id="in_cart" class="span_12">
                    <div class='in_cart_products_count'>Добавьте товары в корзину!</div>
                </div>
            </div>
            <div class="row">
                <div class="cart_box"></div>
            </div>
        </header>
        <main class="shop container">
            <?php foreach ($_ITEMS as $item_id => $item) {
                //var_dump($item);
                ?><a href="#linktoProduct"><container class="shop__item span_2" data-item="<?php echo $item_id; ?>">
                    <img src="<?php echo $item['img']; ?>" alt="<?php echo $item['name']; ?>" class="shop__item_img">
                    <h2 class="shop__item_name mb mt_2"><?php echo $item['name']; ?></h2>
                    <div class="shop__item_price"><?php echo $item['price']; ?> <span class="shop__item_currency"><?php echo $item['currency']; ?></span></div>
                </container></a><a href="#linktoBrand" class="span_2 shop__item_brand"><span data-brand="<?php echo $item['brand']; ?>"><?php echo $item['brand']; ?></span></a><div class="shop__item_buy span_2 alignright"><img src="./img/cart.png" alt="купить <?php echo $item['name']; ?>" data-buy="<?php echo $item_id; ?>" data-name="<?php echo $item['name']; ?>" data-price="<?php echo $item['price']; ?>" data-currency="<?php echo $item['currency']; ?>" width="18px" height="20px"></div><?php
            } ?>
        </main>
    </body>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="script.js"></script>
</html>