'use strict';
 
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');

gulp.task('sass-compile', function () {
  return gulp.src('./sass/**/*.scss')
	.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
	.pipe(sass({outputStyle: 'compressed'}))
	.pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./'))
})

gulp.task('watch', function () {
  gulp.watch('./sass/**/*.scss', gulp.series('sass-compile'));
  gulp.watch(img_MASK, gulp.series('img'));
})